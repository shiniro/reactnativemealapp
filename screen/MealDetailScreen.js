import React, { useEffect, useCallback } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { View, Text, Image, ScrollView, StyleSheet } from 'react-native';
import { HeaderButtons, Item } from 'react-navigation-header-buttons';

import IoniconsHeaderButton from '../components/IoniconsHeaderButton';
import { toogleFavorite } from '../store/actions/meals';

const ListItem = props => {
  return (
    <View style={styles.listItem}>
      <Text>{ props.children }</Text>
    </View>
  );
};

const MealDetailScreen = props => {

  const mealId = props.navigation.getParam('mealId');
  const availableMeals = useSelector(state => state.meals.meals);
  const currentMealIsFavorite = useSelector(state => state.meals.favoriteMeals.some(meal => meal.id === mealId));

  const selectedMeal = availableMeals.find(meal => meal.id === mealId);

  const dispatch = useDispatch();

  const toogleFavoriteHandler = useCallback(() => {
    dispatch(toogleFavorite(mealId));
  }, [dispatch, mealId]);

  useEffect(() => {
    // props.navigation.setParams({
    //   mealTitle: selectedMeal.title
    // });
    props.navigation.setParams({
      toogleFav: toogleFavoriteHandler
    })
  }, [toogleFavoriteHandler]);

  useEffect(() => {
    props.navigation.setParams({
      isFav: currentMealIsFavorite
    })
  }, [currentMealIsFavorite]);

  return (
    <ScrollView>
      <Image source={{ uri: selectedMeal.imageUrl }} style={styles.image} />
      <View style={styles.details}>
        <Text>{ selectedMeal.duration } m</Text>
        <Text>{ selectedMeal.complexity.toUpperCase() }</Text>
        <Text>{ selectedMeal.affordability.toUpperCase() }</Text>
      </View>
      <Text style={styles.title}>Ingridients</Text>
      { selectedMeal.ingredients.map(ingredient => <ListItem key={ingredient}>{ingredient}</ListItem>) }
      <Text style={styles.title}>Steps</Text>
      { selectedMeal.steps.map(step => <ListItem key={step}>{step}</ListItem>) }
    </ScrollView>
  );
};

MealDetailScreen.navigationOptions = navigationData => {

  // const mealId = navigationData.navigation.getParam('mealId');
  const mealTitle = navigationData.navigation.getParam('mealTitle');
  const toogleFavorite = navigationData.navigation.getParam('toogleFav');
  const isFavorite = navigationData.navigation.getParam('isFav');

  // const selectedMeal = MEALS.find(meal => meal.id === mealId);

  return {
    headerTitle: mealTitle,
    headerRight: (
      <HeaderButtons HeaderButtonComponent={IoniconsHeaderButton}>
        <Item
          title="Favorite"
          iconName={isFavorite ? 'ios-star' : 'ios-star-outline'}
          onPress={toogleFavorite}
        />
      </HeaderButtons>
    )
  }
};

const styles = StyleSheet.create({
  image: {
    width: '100%',
    height: 200
  },
  details: {
    padding: 15,
    flexDirection: 'row',
    justifyContent: 'space-around'
  },
  title: {
    fontSize: 22,
    textAlign: 'center'
  },
  listItem: {
    padding: 10,
    marginVertical: 10,
    marginHorizontal: 20,
    borderColor: '#ccc',
    borderWidth: 1,
  }
});

export default MealDetailScreen;