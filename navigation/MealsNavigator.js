import React from 'react';
import { Platform } from 'react-native';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import { createDrawerNavigator } from 'react-navigation-drawer';
import { Ionicons } from '@expo/vector-icons';
import { createMaterialBottomTabNavigator } from 'react-navigation-material-bottom-tabs';

import CategoriesScreen from '../screen/CategoriesScreen';
import CategoryMealsScreen from '../screen/CategoryMealsScreen';
import MealDetailScreen from '../screen/MealDetailScreen';
import FavoritesScreen from '../screen/FavoritesScreen';
import FiltersScreen from '../screen/FiltersScreen';

import Colors from '../constants/Colors';

const defaultStackNavOptions = {
  // mode: 'modal',
  // initialRouteName: 'Categories', // use the first of the object by default
  defaultNavigationOptions: {
    headerStyle: {
      backgroundColor: Platform.OS === 'android' ? Colors.primaryColor : '',
    },
    // headerTitleStyle: {
    //   fontFamily: 'open-sans-bold'
    // },
    // headerBackTitleStyle: {
    //   fontFamily: 'open-sans'
    // },
    headerTintColor: Platform.OS === 'android' ? 'white' : Colors.primaryColor,
  }
};

const MealsNavigator = createStackNavigator(
  {
    Categories: {
      screen: CategoriesScreen,
      navigationOptions: {
        headerTitle: 'Meal Categories'
      }
    },
    CategoryMeals: {
      screen : CategoryMealsScreen,
    },
    MealDetail : MealDetailScreen
  },
  defaultStackNavOptions
);

const FavoriteNavigator = createStackNavigator({
  Favorites: FavoritesScreen,
  MealDetail: MealDetailScreen
}, defaultStackNavOptions);

const tabScreenConfig = {
  Meals: {
    screen: MealsNavigator,
    navigationOptions: {
      tabBarIcon: tabInfo => {
        return (
          <Ionicons
            name="ios-restaurant"
            size={25}
            color={tabInfo.tintColor}
          />
        );
      },
      tabBarColor: Colors.primaryColor, // only work with shifting effect
      tabBarLabel: 'Meals' // Platform.OS === 'android' ? <Text style={{ fontFamily: 'open-sans-bold' }} >Meals</Text> : 'Meals'
    }
  },
  Favorites: {
    screen: FavoriteNavigator,
    navigationOptions: {
      tabBarLabel: 'Favorites',
      tabBarIcon: tabInfo => {
        return (
          <Ionicons name="ios-star" size={25} color={tabInfo.tintColor} />
        );
      },
      tabBarColor: Colors.accentColor
    }
  }
}

const FiltersNavigator = createStackNavigator(
  {
    Filters: FiltersScreen
  },
  defaultStackNavOptions
  // {
  //   navigationOptions: {
  //     drawerLabel: 'Filters !!!',
  //   },
  // }, 
);

const MealsFavTabNavigator = Platform.OS === 'android' 
? createMaterialBottomTabNavigator(tabScreenConfig, {
  activeColor: 'white',
  shifting: true,
  barStyle: {
    backgroundColor: Colors.primaryColor
  }
})
: createBottomTabNavigator(tabScreenConfig, {
    tabBarOptions: {
      // labelStyle: {
      //   fontFamily: 'open-sans-bold'
      // },
      activeTintColor: Colors.accentColor,
    }
  }
);

const MainNavigator = createDrawerNavigator(
  {
    MealsFavs: {
      screen: MealsFavTabNavigator,
      navigationOptions: {
        drawerLabel: 'Meals'
      }
    },
    Filters: FiltersNavigator
  },
  {
    contentOptions: {
      activeTintColor: Colors.accentColor,
      // labelStyle: {
      //   fontFamily: 'open-sans-bold'
      // }
    }
  }
);

export default createAppContainer(MainNavigator);