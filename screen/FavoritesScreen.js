import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { useSelector } from 'react-redux';
import { HeaderButtons, Item } from 'react-navigation-header-buttons';

import IoniconsHeaderButton from '../components/IoniconsHeaderButton';
import MealList from '../components/MealList';

const FavoritesScreen = props => {

  const favMeals = useSelector(state => state.meals.favoriteMeals);

  // const favMeals = availableMeals.filter(meal => meal.id === 'm1' || meal.id === 'm2');

  if (favMeals.length === 0 || !favMeals) {
    return (
      <View style={styles.screen}>
        <Text>No favorite meals found. Start adding some !</Text>
      </View>
    )
  }
  
  return (
    <MealList listData={favMeals} navigation={props.navigation} />
  );
};

FavoritesScreen.navigationOptions = navData => {
  return {
    headerTitle: 'Your Favorites',
    headerLeft: <HeaderButtons HeaderButtonComponent={IoniconsHeaderButton} >
      <Item title="Menu" iconName="ios-menu" onPress={() => {
        navData.navigation.toggleDrawer();
      }}></Item>
    </HeaderButtons>
  }
}

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  }
});
export default FavoritesScreen;